/** Rain Game with sound : drips only **/

window.onload = myGame;
enchant();

function myGame() {
    // New game with a 320x320 pixel canavs, 24 frames/sec.
    var game = new Core(320, 320);
    game.fps = 24;

    // Preload assets.
    game.preload('droplet24.png', 'usertrash32.png', 'sky.png');
    game.preload('drip.wav');

    // Additional game properties (accessible to all helper functions).
    game.caughtDroplets = 0; // How many droplets caught?

    // This object starts counting down once the game is started.
    var timerDown = {
        frameCount: 30 * game.fps, // total needed frames.
        tick: function () { // call tick() every frame.
            if (game.isStarted) {
                this.frameCount -= 1; // count down instead of up.
            }
        }
    };

    // Specify what should happen when the game loads.
    game.onload = function () {
        // Background image.
        var bg = new Sprite(game.width, game.height);
        bg.image = game.assets['sky.png'];
        game.rootScene.addChild(bg);

        // Create a label to show time. (Placed at 0,0 by default.)
        var timeLabel = new Label("Collect droplets as fast as you can.");
        game.rootScene.addChild(timeLabel);

        // Make the collector
        var collector = makeCollector();

        // Add sound as game property.
        game.dripSound = game.assets['drip.wav'];

        // Add an event listener for each frame.
        game.addEventListener(Event.ENTER_FRAME, function () {
            if (timerDown.frameCount > 0) { // if the game is on ...
                // Make a droplet every once in a while.
                if (game.frame % 24 === 0) {
                    makeDroplet(collector);
                }

                // Tick the timer.
                timerDown.tick();

                // Update label.
                timeLabel.text = 'Time remaining: ' +
                    Math.ceil(timerDown.frameCount / game.fps);
            } else if (timerDown.frameCount !== -1) { // Game over?
                // indicate that the "game over" by setting frameCount to -1
                timerDown.frameCount = -1;
                // add final score to screen
                timeLabel.text += '<br>Droplets: ' + game.caughtDroplets;
                // end the game
                game.end();
            }
        });
    };

    // Start the game.
    game.start();

    // ==== myGame helper functions ====================================
    /** Make a droplet with the needed listeners. If droplet collides
     * with collector, remove droplet and give player a point. If
     * droplet goes off screen, remove it.
     * Return a reference to the created droplet. */
    function makeDroplet(collector) {
        var droplet = new Sprite(24, 24);
        droplet.image = game.assets['droplet24.png'];

        // Add a gravity property to the droplet.
        droplet.gravity = 5; // How fast will the droplet drop?

        // Position droplet in random x and random y above screen.
        droplet.x = randomInt(0, game.width - droplet.width);
        droplet.y = randomInt(-1 * game.height, -1 * droplet.height);

        // Add droplet to scene.
        game.rootScene.addChild(droplet);

        // Add an event listener to the droplet Sprite to move it.
        droplet.addEventListener(Event.ENTER_FRAME, function () {
            // Game starts when first drop starts falling.
            game.isStarted = true;

            // Move.
            if (droplet.y > game.height) { // if below canvas, remove.
                game.rootScene.removeChild(droplet);
            } else { // else move down as usual.
                droplet.y += droplet.gravity;
            }

            // Check for collision.
            if (droplet.intersect(collector)) {
                game.rootScene.removeChild(droplet);
                catchDroplet();
            }
        });

        return droplet;
    }

    /** Make a collector with the needed listeners.
     * Return a reference to the created object. */
    function makeCollector() {
        var collector = new Sprite(32, 32);
        collector.image = game.assets['usertrash32.png'];

        // Add a speed property to the collector.
        collector.speed = 4; // How fast will collector move?

        collector.x = (game.width + collector.width) / 2;
        collector.y = game.height - collector.height;

        // Add collector to scene.
        game.rootScene.addChild(collector);

        // Event listeners for collector.
        collector.addEventListener(Event.ENTER_FRAME, function () {
            // Move.
            if (game.input.right && !game.input.left) {
                collector.x += collector.speed;
            } else if (game.input.left && !game.input.right) {
                collector.x -= collector.speed;
            }

            // Check limits.
            if (collector.x > game.width - collector.width) {
                collector.x = game.width - collector.width;
            } else if (collector.x < 0) {
                collector.x = 0;
            }
        });

        return collector;
    }

    /** Give the player a point for catching a drop. */
    function catchDroplet() {
        game.dripSound.play();
        game.caughtDroplets += 1;
    }

    /** Generate a random integer between low and high (inclusive). */
    function randomInt(low, high) {
        return low + Math.floor((high + 1 - low) * Math.random());
    }
    // ==== End myGame helper functions ================================
}